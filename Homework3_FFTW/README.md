-------------------------------------------------------------------
# Project Homework 3, Dec 2021
Authors:
Zheng Zheng    zheng.zheng@epfl.ch
Tian Yang      tian.yang@epfl.ch

-------------------------------------------------------------------
# Instructions to compile the program

The simplest way to compile the program:

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```
The `FFTW` library is used by default. If you want to neglect it, you can compile with:

```
$ cmake .. -USE_FFTW=OFF
```
Four executable files `particles`, `test_fft`, `test_heat_equation`, `test_kepler` will be generated in `/build/src` folder.

Please note that the `googletest` should be installed in the `/src` folder.
-------------------------------------------------------------------
# Usage of each executable

`test_kepler` is relevant to `planet`

`test_fft` is relevant to Exercise 3. It tests (1) Fast Fourier transform; (2) inverse Fourier transform; and (3) computation of wave number.

`test_heat_equation` is relevant to Exercise 4.2 - 4.4. It tests (1) uniform zero heat source; (2) sinusoidal heat source; and (3) delta heat source.
	
	Note: To execute `test_heat_equation`, one should copy three input files (`input_delta128.csv`, `input_sin64.csv`, `input_uniform64.csv`) 
	
	into `/build/src` folder. They are provided in `/tools`.

`particles` is used to create usr's own simulation. Specific steps to execuate it are given below:

	Step 1: generate 'input.csv' file with `/tools/generate_input.py`. 
		
		eg: "$ python3 generate_input.py --size 64 --filename input.csv --heatsource uniform --halflength 1.0 --temperature 1.0 --massdensity 1.0 --heatcapacity 1.0 --kappa 1.0"
		
		One can type `$ python3 generate_input.py -h` in console to see the detailed usage 
	
	Step 2: execute `particles` with correct arguments:
	
		For material_points:
	
		"$ ./particles nsteps freq filename particle_type timestep (length heatboundary_type)" 
		length and heatboundary_type are only needed for the material_point. 
		
		eg: $ ./particles 1000 10 input_sin64.csv material_point 0.0001 2 periodic 
		
		where  "heatboundary_type" can be "periodic" or "fixed", "fixed" is only used in ex 4.5.
	
	Step 3: View output file in `/dumps` with Paraview software
	
	
# Visualization of simulation results in Paraview (similar steps as in exercise week 7)

Step 1: The dumps observable files should be loaded into Paraview. Then, it is important to set the `field Delimiter Character` to a space instead of `,`. Click on `Apply`.

Step 2: Use the `Table To Points` filter to convert the data points to correct coordinates. In our case, it is quite obvious: the x-coordinates corresponds to the `X`, the y-coordinates corresponds to the `Y`  etc.. Click on `Apply` always.

Step 3: The `T` field represents the temperature field and the `H` field shows the heat source. Rescale the temperature range to have a better visualization. It would also be nice to visualize heat source (H) and temperature (T) fields with a filter as `Delaunay 2D` or `Point Volume Interpolator`.	

Recommendation: run the simulation of radial heat source and enjoy the animation in Paraview.

	Step 1: `$ /tools/python3 generate_input.py --size 256 --filename input_radial256.csv --heatsource radial --radius 0.5`
	
	Step 2: copy `input_radial256.csv` into `/build/src`

	Step 3: `$ ./build/src/particles 5000 25 input_radial256.csv material_point 0.000001 2 fixed`
-------------------------------------------------------------------
# Answer to Ex.1

There are three kinds of particle 'planet', 'ping_pong', and 'material_point". They all inherit from the 'particle' class and they store some essential properties of particles.

The 'System' object contains the address of all particles. The 'SystemEvolution' object contains all the 'compute' for all 'particles' and is used for the evolution of 'System'. They are based on 'particles'.

Three 'factory' inherit from the same 'particles_factory_interface' manipulate three types of particles accordingly. They control the creation of 'particles' in 'System' and use 'SystemEvolution' to evolve 'System'. They are the direct interface for users.

 





