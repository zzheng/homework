#include "material_point.hh"

/* -------------------------------------------------------------------------- */
void MaterialPoint::printself(std::ostream& stream) const {
  //Particle::printself(stream);
  stream << " " << position << temperature << " " << heat_rate;
}

/* -------------------------------------------------------------------------- */

void MaterialPoint::initself(std::istream& sstr) {
  //Particle::initself(sstr);
  sstr >> position >> temperature  >> heat_rate >> rho >> heat_capacity >> kappa;
}
