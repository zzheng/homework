#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {

  UInt np = system.getNbParticles(); 		// number of all particles
  UInt size = UInt (sqrt(np));  			// number of particles in one direction

  Matrix<complex> theta(size) ; 			// stores the temperture of all particles
  Matrix<complex> heat_source(size); 		// stores the volumetric heat source of all particles
  Matrix<complex> DthetaDt(size); 			// d(theta)/dt

  Matrix<complex> theta_fft(size); 			// after FFT
  Matrix<complex> heat_source_fft(size); 	// after FFT
  Matrix<complex> DthetaDt_fft(size); 		// after FFT

  Matrix<std::complex<int>> wave_number = FFT::computeFrequencies(size); // wave numbers of all particles

  // Initialize "theta" and "heat_source"
  for(auto&& entry: index(theta)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    UInt id = i*size + j;  // particle is stored in row-major order matrix
    
    auto& par = dynamic_cast<MaterialPoint&>(system.getParticle(id));
    theta(i,j) = complex(par.getTemperature(), 0);
    heat_source(i,j) = complex(par.getHeatRate(), 0);
  }

  // Step-1: FFT
  theta_fft = FFT::transform(theta);
  heat_source_fft = FFT::transform(heat_source);

  // Step-2: compute "DthetaDt_fft"
  for (auto && entry : index(DthetaDt_fft)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    UInt id = i*size + j;
    
    auto& par = dynamic_cast<MaterialPoint&>(system.getParticle(id));
    Real rho = par.getMassDensity();
    Real hc = par.getHeatCapacity();
    Real kappa = par.getHeatConductivity();
    
    Real qx = 2*M_PI/L*wave_number(i,j).real();
    Real qy = 2*M_PI/L*wave_number(i,j).imag();
    DthetaDt_fft(i,j) = 1/(rho * hc) * (heat_source_fft(i,j) - kappa*theta_fft(i,j)*(pow(qx,2) + pow(qy,2)));
  }

  // Step-3: IFFT to physical space
  DthetaDt = FFT::itransform(DthetaDt_fft);

  // Step-4: Euler integration for dt and update temperture
  for (auto && entry : index(DthetaDt)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    UInt id = i * size + j;
    
    auto& par = dynamic_cast<MaterialPoint&>(system.getParticle(id));
    par.getTemperature() += val.real() * dt;
  }

}

/* -------------------------------------------------------------------------- */
