#include "compute_heat_boundary.hh"
/* -------------------------------------------------------------------------- */

void Compute_heat_boundary::compute(System& system) {

  UInt size = std::sqrt(system.getNbParticles());
  UInt id;
  
  if (this->hb_type == "periodic"){}
  
  // impose the boundary conditions that are 0 on the boundary 
  else if(this->hb_type == "fixed"){
    for(int i = 0; i < size; i++){
      id = i; // particles that y = ymin
      auto& par = dynamic_cast<MaterialPoint&>(system.getParticle(id));
      par.getTemperature() = 0.0;
    }

    for(int j = 0; j < size; j++){
      id = j*size; // particles that x = xmin
      auto& par = dynamic_cast<MaterialPoint&>(system.getParticle(id));
      par.getTemperature() = 0.0;
    }
  }
}
/* -------------------------------------------------------------------------- */
