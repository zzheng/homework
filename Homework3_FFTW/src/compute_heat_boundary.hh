#ifndef GENERATE_INPUT_PY_COMPUTE_HEAT_BOUNDARY_H
#define GENERATE_INPUT_PY_COMPUTE_HEAT_BOUNDARY_H
/* -------------------------------------------------------------------------- */
#include <utility>
#include "compute.hh"
#include "material_point.hh"
/* -------------------------------------------------------------------------- */

class Compute_heat_boundary : public Compute{

public:
  Compute_heat_boundary(std::string hb_type0){
    this->hb_type = std::move(hb_type0);
  }
  
  void set_hb_type(std::string hb_type0){
    this->hb_type = std::move(hb_type0);
  }

  void compute(System & system) override;
  
private:
  std::string hb_type;
};

/* -------------------------------------------------------------------------- */
#endif  // GENERATE_INPUT_PY_COMPUTE_HEAT_BOUNDARY_H
