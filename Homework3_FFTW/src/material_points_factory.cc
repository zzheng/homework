#include "material_points_factory.hh"
#include "compute_temperature.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include <cmath>
#include <iostream>
#include "compute_heat_boundary.hh"
/* -------------------------------------------------------------------------- */

std::unique_ptr<Particle> MaterialPointsFactory::createParticle() {
  return std::make_unique<MaterialPoint>();
}

/* -------------------------------------------------------------------------- */

SystemEvolution&
MaterialPointsFactory::createSimulation(std::vector<std::string> simu_argv) {
  std::string  fname = simu_argv[0];
  Real timestep;
  std::stringstream(simu_argv[1]) >> timestep;
  Real length;
  std::stringstream(simu_argv[2]) >> length;
  std::string  hb_type = simu_argv[3];
  this->system_evolution =
      std::make_unique<SystemEvolution>(std::make_unique<System>());

  CsvReader reader(fname);
  reader.read(this->system_evolution->getSystem());

  // check if it is a square number
  auto N = this->system_evolution->getSystem().getNbParticles();
  int side = std::sqrt(N);
  if (side * side != N)
    throw std::runtime_error("number of particles is not square");

  auto temperature = std::make_shared<ComputeTemperature>(timestep, length);
  this->system_evolution->addCompute(temperature);
  auto heat_boundary = std::make_shared<Compute_heat_boundary>(hb_type);
  this->system_evolution->addCompute(heat_boundary);

  return *system_evolution;
}

/* -------------------------------------------------------------------------- */

ParticlesFactoryInterface& MaterialPointsFactory::getInstance() {
  if (not ParticlesFactoryInterface::factory)
    ParticlesFactoryInterface::factory = new MaterialPointsFactory;

  return *factory;
}

/* -------------------------------------------------------------------------- */
