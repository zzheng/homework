#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

/*****************************************************************/
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2.0, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2.0, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, itransform) {
  UInt N = 512;
  Matrix<complex> m(N);

  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    
    if (i == 1 && j == 0) {
      val.real(Real(N * N / 2));
      val.imag(0.0); 
    }
    else if (i == N - 1 && j == 0){
      val.real(Real(N * N / 2));
      val.imag(0.0); 
    }
    else{
      val.real(0.0);
      val.imag(0.0);
    }
  }

  Matrix<complex> res = FFT::itransform(m);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    
    ASSERT_NEAR(val.real(), cos(i*k), 1e-10);
    ASSERT_NEAR(val.imag(), 0.0, 1e-10);
  }
}
/*****************************************************************/
TEST(FFT, frequency){
  // Here, we test if FFT(du/dx) = 2*pi*wave_number_x/T_x * FFT(U)
  // and if FFT(du/dy) = 2*pi*wave_number_y/T_y * FFT(U)
  
  UInt N = 512;
  complex I(0.0, 1.0);

  Real T_x = 1 * M_PI; 			// x belongs to (0, T_x)
  Real T_y = 4 * M_PI; 			// y belongs to (0, T_y)

  Matrix<Real> x_grid(N);
  Matrix<Real> y_grid(N);
  Matrix<complex> u(N); 		// u(x,y) = sin(2x)+ cos(4y)
  Matrix<complex> dudx_ana(N);  // analytical solution of du/dx
  Matrix<complex> dudy_ana(N);  // analytical solution of du/dy
  Matrix<complex> dudx_fft(N);  // FFT solution of du/dx
  Matrix<complex> dudy_fft(N);  // FFT solution of du/dy

  // initialize x_grid
  for (auto && entry: index(x_grid)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = j* T_x/N;
  }

  // initialize y_grid
  for (auto && entry: index(y_grid)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = i* T_y/N;
  }

  // u(x,y) = sin(2x)+cos(4y)
  for (auto&& entry : index(u)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    
    Real x_ij = x_grid(i,j);
    Real y_ij = y_grid(i,j);
    val = sin(2 * x_ij) + cos(4 * y_ij);
  }

  // analytical du/dx = 2*cos(2x)
  for (auto&& entry : index(dudx_ana)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    
    Real x_ij = x_grid(i,j);
    val = 2* cos(2 * x_ij);
  }

  // analytical du/dy = -4*sin(4y)
  for (auto&& entry : index(dudy_ana)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    Real y_ij = y_grid(i,j);
    val = -4 * sin(4 * y_ij);
  }

  auto u_fft = FFT::transform(u);
  auto wave_number = FFT::computeFrequencies(N);

  // FFT solution of du/dx
  for (auto&& entry : index(dudx_fft)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    
    val = 2.0 * M_PI / T_x * wave_number(i,j).real() * I * u_fft(i,j);
  }
  
  dudx_fft = FFT::itransform(dudx_fft);

  // FFT solution of du/dy
  for (auto&& entry : index(dudy_fft)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    
    val = 2.0 * M_PI / T_y * wave_number(i,j).imag() * I * u_fft(i,j);
  }
  
  dudy_fft = FFT::itransform(dudy_fft);

  // Testing equality
  for(int i = 0; i < N; i++)
  {
    for(int j = 0; j < N; j++)
    {
      ASSERT_NEAR(std::abs(dudx_fft(i,j) - dudx_ana(i,j)), 0, 1e-11);
      ASSERT_NEAR(std::abs(dudy_fft(i,j) - dudy_ana(i,j)), 0, 1e-11);
    }
  }
}

