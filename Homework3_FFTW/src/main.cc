#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "my_types.hh"
#include "ping_pong_balls_factory.hh"
#include "material_points_factory.hh"
#include "planets_factory.hh"
#include "system.hh"
#include <sys/stat.h>

/* -------------------------------------------------------------------------- */
#include <cstdlib>
#include <iostream>
#include <sstream>
/* -------------------------------------------------------------------------- */
void checkargv(int argc, char ** argv);

int main(int argc, char** argv) {
  // check argc and argv
  checkargv(argc, argv);
  // create the "dumps" fold to store output file
  mkdir("dumps", 0777);
  // the number of steps to perform
  Real nsteps;
  std::stringstream(argv[1]) >> nsteps;
  // freq to dump
  int freq;
  std::stringstream(argv[2]) >> freq;
  // init file
  std::string filename = argv[3];
  // particle type
  std::string type = argv[4];
  // timestep
  std::string timestep = argv[5];

  std::vector<std::string> simu_argv; // will be passed to factory.createsimulation()

  if (type == "planet") {
    PlanetsFactory::getInstance();
    simu_argv = {filename, timestep};
  }
  else if (type == "ping_pong") {
    PingPongBallsFactory::getInstance();
    simu_argv = {filename, timestep};
  }
  else if (type == "material_point") {
    MaterialPointsFactory::getInstance();
    std::string length = argv[6];
    std::string heatboundary_type = argv[7];
    simu_argv = {filename, timestep, length, heatboundary_type};
  }
  else {
    std::cout << "Unknown particle type: " << type << std::endl;
    std::exit(EXIT_FAILURE);
  }

  ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();

  SystemEvolution& evol = factory.createSimulation(simu_argv);

  evol.setNSteps(nsteps);
  evol.setDumpFreq(freq);

  evol.evolve();

  return EXIT_SUCCESS;
}

void checkargv(int argc, char ** argv){
  std::string particle_type = argv[4];
  if (particle_type == "planet") {
    if (argc != 6) {
      std::cout << "ERROR: The number of argv is not correct" << std::endl;
      std::cout << "Usage for planet: " << argv[0]
                << " nsteps dump_freq input.csv planet timestep"
                << std::endl;
      std::exit(EXIT_FAILURE);
    }
  }
  else if(particle_type == "ping_pong"){
    if (argc != 6) {
      std::cout << "ERROR: The number of argv is not correct" << std::endl;
      std::cout << "Usage for ping_pong: " << argv[0]
                << " nsteps dump_freq input.csv ping_pong timestep"
                << std::endl;
      std::exit(EXIT_FAILURE);
    }
  }
  else if (particle_type == "material_point"){
    if (argc != 8) {
      std::cout << "ERROR: The number of argv is not correct" << std::endl;
      std::cout << "Usage for material_point: " << argv[0]
                << " nsteps dump_freq input.csv material_point timestep length boundary_type"
                << R"(where "heatboundary_type" should be "fixed" or "periodic" )"
                << std::endl;
      std::exit(EXIT_FAILURE);
    }
    std::string heatboundary_type = argv[7];
    if (!(heatboundary_type == "periodic" || heatboundary_type == "fixed")){
      std::cout << "ERROR: The heat boundary type for \"material_point\" is not correct" << std::endl;
      std::cout << R"("heatboundary_type" should be "fixed" or "periodic" )"<< std::endl;
    }

  }
  else{
    std::cout << "ERROR: Particle type should be in {planet, ping_pong, material_point}"<<std::endl;
    std::cout << "Usage: " << argv[0]
              << " nsteps dump_freq input.csv material_point timestep (length heatboundary_type)"
              << std::endl;
    std::cout << R"("length" "heatboundary_type" are only needed for "material_point")";
    std::exit(EXIT_FAILURE);
  }
}
