import numpy as np
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("number", help="number of particles", type=int)
parser.add_argument("filename", help="name of generated input file")
parser.add_argument("type", help="the type of heat source", type = str, action="store", choices=["uniform", "sin"])
args = parser.parse_args()

N = args.number
type = args.type

L = 2
x = np.linspace(-L/2, L/2, N+1)[:-1]
y = x.copy()

mesh = np.meshgrid(x, y)
positions = np.zeros((N, N, 3))
positions[:, :, 0] = mesh[1]
positions[:, :, 1] = mesh[0]



if type == "uniform":
     temperature = np.ones((N, N))
     heat_source = np.zeros_like(temperature)
elif type == "sin":
     temperature = np.zeros((N, N))
     heat_source = np.zeros_like(temperature)
     for i in range(0, N):
          for j in range(0, N):
               heat_source[i,j] = pow(np.pi*2/L,2) * np.sin(np.pi*2/L* positions[i,j,0])
               temperature[i,j] = np.sin(np.pi*2/L* positions[i,j,0])
 




temperature = temperature.reshape((N*N, 1))
heat_source = heat_source.reshape((N*N, 1))

temperature_rate = np.zeros_like(heat_source)
positions = positions.reshape((N*N, 3))
velocity = np.zeros_like(positions)
force = velocity.copy()
mass = np.ones((N*N, 1))

print(temperature.shape)
print(heat_source.shape)
print(positions.shape)
print(velocity.shape)
print(force.shape)
print(mass.shape)


headers = ["X", "Y", "Z",
           "velX", "velY", "velZ",
           "fX", "fY", "fZ",
           "mass",
           "temperature",
           "temperature_rate",
           "heat_source"]

file_data = np.hstack((positions, velocity, force, mass,
                       temperature, temperature_rate, heat_source))
np.savetxt(args.filename, file_data, delimiter=" ",
           header='#'+" ".join(headers), comments="")

