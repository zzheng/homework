#ifndef COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH
#define COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH

#include "compute.hh"
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>

class ComputeTemperatureFiniteDifferences : public Compute {
public:
  ComputeTemperatureFiniteDifferences() { }

  void compute(System& system) override;
  Eigen::SparseMatrix<double> assembleLinearOperator(System& system);
  Eigen::VectorXd  assembleRightHandSide(System& system);

	//! return the heat conductivity
	Real& getConductivity() { return conductivity; };
	//! return the heat capacity
	Real& getCapacity() { return capacity; };
	//! return the heat capacity
	Real& getDensity() { return density; };
	//! return the characteristic length of the square
	Real& getL() { return L; };
	//! return the characteristic length of the square
	Real& getDeltat() { return delta_t; };

    bool implicit = true;

//private:
public: // changed to public for pybind 
    Real conductivity;
    Real capacity;
    Real density;
    // side length of the problem
    Real L;
    Real delta_t;
};

#endif  // COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH
