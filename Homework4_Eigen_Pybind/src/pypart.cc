#include <functional>
#include <iostream>
#include <pybind11/pybind11.h>
#include <memory>

#include "particles_factory_interface.hh" 
#include "material_points_factory.hh"
#include "planets_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "csv_writer.hh"
#include "compute.hh" 
#include "compute_temperature.hh"
#include "compute_interaction.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "my_types.hh" 
#include "fft.hh"
#include "system_evolution.hh"
#include "compute_temperature_finite_differences.hh"

namespace py = pybind11;

PYBIND11_MODULE(pypart, m) {
    m.doc() = "pybind of the Particles project";
    
	/* ------------------------Factory Interface------------------------------ */
 	
    /* Particles Factory Interface */
    py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface",
                                          py::dynamic_attr()) // to allow new members to be created dynamically
        .def("createSimulation", py::overload_cast<const std::string&, Real, py::function>
            (&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
        .def("getInstance", &ParticlesFactoryInterface::getInstance,
             py::return_value_policy::reference)
        .def_property_readonly("system_evolution",
             &ParticlesFactoryInterface::getSystemEvolution);


    /* Material Points Factory Interface */
    py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(m, "MaterialPointsFactory",
                                                                 py::dynamic_attr())
        .def("getInstance", &MaterialPointsFactory::getInstance,
             py::return_value_policy::reference);


    /* Planets Factory Interface */
    py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory",
                                                          py::dynamic_attr())
        .def("getInstance", &PlanetsFactory::getInstance,
             py::return_value_policy::reference);


    /* PingPongBalls Factory Interface */
    py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(m, "PingPongBallsFactory",
                                                                py::dynamic_attr())
    	.def("getInstance", &PingPongBallsFactory::getInstance,
             py::return_value_policy::reference);
	/* -------------------------------------------------------------------------- */
	

	/* --------------------------Compute Interface------------------------------- */
	
    /* Compute Interface */
    py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute"); //using shared pointers as object holder

    /* Compute Interaction Interface */
    py::class_<ComputeInteraction, Compute,
                    std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction",
                                                         py::dynamic_attr());

    /* Compute Gravity Interface */
    py::class_<ComputeGravity, ComputeInteraction,
                std::shared_ptr<ComputeGravity>>(m, "ComputeGravity",
                                                 py::dynamic_attr())
    	.def(py::init<>()) // constructor
    	.def("compute", &ComputeGravity::compute, py::return_value_policy::reference)
    	.def("setG", &ComputeGravity::setG);


	/* Compute Temperature Interface */
    py::class_<ComputeTemperature, Compute,
                    std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature",
                                                         py::dynamic_attr())
        .def(py::init<>())
        .def("compute", &ComputeTemperature::compute, py::return_value_policy::reference)
        .def_readwrite("conductivity", &ComputeTemperature::conductivity)
        .def_readwrite("capacity", &ComputeTemperature::capacity)
        .def_readwrite("density", &ComputeTemperature::density)
        .def_readwrite("L", &ComputeTemperature::L)
        .def_readwrite("deltat", &ComputeTemperature::delta_t);
    
    /* Class ComputeTemperatureFD */
    py::class_<ComputeTemperatureFiniteDifferences, Compute,
                    std::shared_ptr<ComputeTemperatureFiniteDifferences>>(m, "ComputeTemperatureFiniteDifferences",
                                                         py::dynamic_attr())
        .def(py::init<>())
        .def("compute", &ComputeTemperatureFiniteDifferences::compute,
             py::return_value_policy::reference)
        .def_readwrite("conductivity", &ComputeTemperatureFiniteDifferences::conductivity)
        .def_readwrite("capacity", &ComputeTemperatureFiniteDifferences::capacity)
        .def_readwrite("density", &ComputeTemperatureFiniteDifferences::density)
        .def_readwrite("L", &ComputeTemperatureFiniteDifferences::L)
        .def_readwrite("deltat", &ComputeTemperatureFiniteDifferences::delta_t);
        
    /* ComputeVerletIntegration Interface */
    py::class_<ComputeVerletIntegration, Compute,
                std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration",
                                                           py::dynamic_attr())
    	.def(py::init<Real>())
    	.def("compute", &ComputeVerletIntegration::compute, py::return_value_policy::reference)
    	.def("setDeltaT", &ComputeVerletIntegration::setDeltaT)
    	.def("addInteraction", &ComputeVerletIntegration::addInteraction);
	/* -------------------------------------------------------------------------- */


	/* --------------------------Other classes Interface------------------------------- */

	/* System Interface */
    py::class_<System>(m, "System");

    /* System Evolution Interface */
    py::class_<SystemEvolution, std::unique_ptr<SystemEvolution>>(m, "SystemEvolution",
                                                                  py::dynamic_attr())
        .def(py::init([](std::unique_ptr<System>()){return std::unique_ptr<SystemEvolution>();}),
             py::return_value_policy::move)
        .def("evolve", &SystemEvolution::evolve)
        .def("addCompute", &SystemEvolution::addCompute)
        .def("getSystem", &SystemEvolution::getSystem, py::return_value_policy::reference)
        .def("setNSteps", &SystemEvolution::setNSteps)
        .def("setDumpFreq", &SystemEvolution::setDumpFreq);
	
    /* CsvWriter Interface */
    py::class_<CsvWriter, Compute, std::shared_ptr<CsvWriter>>(m, "CsvWriter")
        .def(py::init<const std::string &>()) // constructor
        .def("write", &CsvWriter::write);
}
/* -------------------------------------------------------------------------- */

