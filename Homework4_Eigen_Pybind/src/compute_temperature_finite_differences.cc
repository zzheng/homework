#include "compute_temperature_finite_differences.hh"
#include "material_point.hh"
#include "matrix.hh"

void ComputeTemperatureFiniteDifferences::compute(System& system) {
    UInt size = system.getNbParticles();

    auto LO = ComputeTemperatureFiniteDifferences::assembleLinearOperator(system);
    auto RH = ComputeTemperatureFiniteDifferences::assembleRightHandSide(system);

    LO.makeCompressed();

    // construct a sparse LV solver
    Eigen::SparseLU<Eigen::SparseMatrix<double>> solver;

    // do the LU factorization
    solver.compute(LO);

    if (solver.info() != Eigen::Success) {
        // decomposition failed
        std::cout << " LU decompostion failed" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    Eigen::VectorXd x(size);
    x = solver.solve(RH);

    if (solver.info() != Eigen::Success) {
        // solving failed
        std::cout << " solving failed" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    for (int p = 0; p < size; ++p) {
        auto& par = dynamic_cast<MaterialPoint&>(system.getParticle(p));
        par.getTemperature() = x(p);
    }

}

Eigen::SparseMatrix<double> ComputeTemperatureFiniteDifferences::assembleLinearOperator(
    System& system)  {
    UInt size = system.getNbParticles();
    int N = std::sqrt(size);

    Eigen::SparseMatrix<double> LO(size,size);
    LO = Eigen::VectorXd::Constant(size,1.0).asDiagonal();

    Real a = this->L / N;
    Real factor = delta_t * conductivity / density / capacity;

    UInt index;
    for (int j = 0; j < N; ++j) {
        for (int i = 0; i < N; ++i) {
            index = j*N+i; //column major

            int ip1, jp1, is1, js1;
            ip1 = i + 1; jp1 = j + 1;
            is1 = i - 1; js1 = j - 1;

            // periodic conditions
            if (is1 < 0){
                is1 = N - 1;
            }
            if (js1 < 0) {
                js1 = N - 1;
            }
            if (ip1 >= N) {
                ip1 = 0;
            }
            if (jp1 >= N) {
                jp1 = 0;
            }

            LO.coeffRef(index, j*N + is1) -= factor/(a*a);
            LO.coeffRef(index, js1*N + i) -= factor/(a*a);
            LO.coeffRef(index, j*N + ip1) -= factor/(a*a);
            LO.coeffRef(index, jp1*N + i) -= factor/(a*a);
            LO.coeffRef(index, j*N + i) -= -4*factor/(a*a);
        }
    }
    return LO;
}

Eigen::VectorXd  ComputeTemperatureFiniteDifferences::assembleRightHandSide(
    System& system)  {
    UInt size = system.getNbParticles();
    Eigen::VectorXd RH(size);

    Real factor = delta_t / density / capacity;

    for (int p = 0; p < size; ++p) {
        auto& par = dynamic_cast<MaterialPoint&>(system.getParticle(p));
        RH(p) = factor * par.getHeatSource() + par.getTemperature();
    }
    return RH;
}
