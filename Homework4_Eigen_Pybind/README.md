-------------------------------------------------------------------
# Project Homework 4, Jan 2022
Authors:
Zheng Zheng    zheng.zheng@epfl.ch
Tian Yang      tian.yang@epfl.ch

-------------------------------------------------------------------
# Instructions to compile the program

The simplest way to compile the program:

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```
The `FFTW` library is used by default. If you want to neglect it, you can compile with:

```
$ cmake .. -USE_FFTW=OFF
```
Four executable files `particles`, `test_fft`, `test_fd`, `test_kepler` will be generated in `/build/src` folder. The `main.py` file will be automatically cpoied in `/build/src` and to be launched for Pybind.

Please note that the `googletest` and `pybind11` should be installed in the `/src` folder.
-------------------------------------------------------------------
# Create input file

You can use the python file `generate_FD_input.py` in `/tools` folder to create input files:

```
$ python3 generate_FD_input.py number filename type
```
For example:

```
$ python3 generate_FD_input.py 64 input_sin_64.csv sin
```
Two pre-generated input file `input_sin_64.csv` and `input_uniform_64.csv` are also uploaded in `/tools` folder
-------------------------------------------------------------------
# Usage of each executable
Pretty much as the steps in HW3, the main difference here is `test_fd`. This new test is designed to test the `FiniteDifferences` method with the first two examples in HW3.

To test the code:

```
$ ./test_fd
```

To create simulation via the executable file `particle`: 
```
$ ./particles 10 1 input_sin_64.csv material_point 0.001
```
Note: In `material_points_factory.cc` file, `MaterialPointsFactory::createDefaultComputes` function uses `FiniteDifferences` method by default.  You can change it to `FFT` method in this source file.
-------------------------------------------------------------------
# Pybind

In `/build/src` folder, the particle codes could be launched via main.py:

```
$ python3 main.py nsteps freq filename particle_type timestep
```
for help mode, simply type: 

```
$ python3 main.py -h
```
Note that the dumps folder should be manually created in `/build/src` via command mkdir dumps

As an example of launch:

```
$ python3 main.py 10 1 input_sin_64.csv material_point 0.001
```

The resulting .csv files could then be visualized in Paraview (For details, see the `README` of HW3).

Note: `main.py` uses `FFT` method for material points by default. You can change it to `FiniteDifferences` method in the python file.
-------------------------------------------------------------------
# Answer to Ex.3

The `createSimulation` function is overloaded with a functor argument, so that the `createComputes` function could receive the python functor with the functionality one wants. The python functor includes initializations and adding compute objects. The main idea of overloading is to give more flexibilities to future users to easily change computing objects, by only changing few things in Python and without going deeper in original C++ codes.

 





