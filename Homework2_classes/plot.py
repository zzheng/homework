##################################################################################
# HW2 - Classes in C++ - Oct 2021

# Students:
# Zheng Zheng    zheng.zheng@epfl.ch
# Tian Yang      tian.yang@epfl.ch
##################################################################################

import matplotlib.pyplot as plt
import os


if __name__ == '__main__':

    x = []
    numerical = []
    analytic = []
    
    fig = plt.figure()
    axe = fig.add_subplot(1, 1, 1)
    
    ########Get the file path 
    workpath = os.path.dirname(__file__)
    filepath = os.path.join(workpath, 'build/src/result.txt')
    
    with open(filepath, 'r') as f:
        lines = f.readlines()
    
    ########Read the file by lines
    for line in lines:
        words = line.split()
        x.append(float(words[2]))
        numerical.append(float(words[5]))
        
        #Check if there are analytic solutions
        if len(words) > 7:
            analytic.append(float(words[7]))

    #Plot
    axe.plot(x, numerical, marker='o', label='Numerical')
    
    if analytic != []:
        axe.plot(x, analytic, label='Analytical')
        
    #Set the figure  
    axe.set_xlabel(r'$k$')
    axe.set_ylabel(r'Series')
    axe.legend()
    plt.show()
