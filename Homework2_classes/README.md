-------------------------------------------------------------------
# Project Homework 2, Oct 2021
Authors:
Zheng Zheng    zheng.zheng@epfl.ch
Tian Yang      tian.yang@epfl.ch

-------------------------------------------------------------------
# Division of the work:
The strategy we used to divide the work is:
One student takes care of the Ex1 to Ex4, while another student optimizes the code by doing Ex5 and apply it to Ex6.

In fact, we met several difficulties for this project following the above approach. Especially, one student has to wait for another to complete Ex1 to Ex4, then he can step further for Ex5 to Ex6. It means we can not code the whole project simultaneously. 

If we had a second chance for this project, we agree that maybe the best way for the division of work is to hold a discussion before the actual work starts, where the members and methods of each class will be determined. Of course, the inputs, returns of each function also need to be decided. In other words, we complete the ".h" file together at first. Then each student will only take care of the implementation of some specific classes. And every programmer can carry on their work at the same time. For exemple, in this project one student can work with the "Series" classes and the other focuses on the "DumperSeries" classes, as long as they keep their interfaces consistent with each other.

-------------------------------------------------------------------
# Answer Ex 6.4
To have the precision of 2 digits:

N for x^3: 1200 iterations

N for cosx: 700 iterations

N for sinx: 200 iterations

-------------------------------------------------------------------
# Codes and Dependencies:
main.cpp

Series.cpp

Series.h

ComputeArithmetic.cpp 

ComputeArithmetic.h : depends on "Series.h"

ComputePi.cpp

ComputePi.h : depends on "Series.h"

DumperSeries.h : depends on "Series.h"

PrintSeries.cpp 

PrintSeries.h : depends on "DumperSeries.h"

WriteSeries.cpp

WriteSeries.h : depends on "DumperSeries.h"

RiemannIntegral.cpp

RiemannIntegral.h : depends on "Series.h"

-------------------------------------------------------------------
# Execution:

1. Arguments for "Pi" and "Arithmetic"

Pi/Arithmetic, frequency, maxiter, precision, Print/Write, seperator

(seperator is optional;  "," for ".csv"; "|" for ".psv"; " " for ".txt")

eg:

./main Pi 1 100 4 Print

./main Arithmetic 2 200 4 Write

./main Arithmetic 2 200 4 Write ,

2. Arguments for "Riemann"

Riemann, a, b, N, precision, f

(with f: x3, cosx, sinx)

eg:

./main Riemann 0 1 1000 3 x3

./main Riemann 0 3.14159 1000 3 cosx

-------------------------------------------------------------------




