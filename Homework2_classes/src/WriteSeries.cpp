#include "WriteSeries.h"
#include <iomanip>
#include <iostream>
#include <fstream>
#include <cmath>


WriteSeries ::WriteSeries(unsigned int f, unsigned int m, Series &s, std::string sep):DumperSeries(s) {
    this->frequency = f;
    this->maxiter = m;
    this->seperator = sep;
}

void WriteSeries ::dump(std::ostream & os) {
    double res1 = 0; // res1 stores current result
    double res0 = 0; // res0 stores last result 
    double converg = 0; // converg: convergence

    std::ofstream outfile;
    std::string filename = this->setSeparator(); //Decide file type
    outfile.open(filename, std::ios::trunc); 

    int i = 0;
    while ( i * this->frequency + 1 < this->maxiter){
        res1 = this->series.compute(i * this->frequency + 1);
        converg = res1 - res0;
        double analytic = this->series.getAnalyticPrediction(); //analytic: analytical result
        
        //If no analytic solution
        if (std::isnan(analytic)){
            outfile << "k = " << i * this->frequency + 1 << " result = " << res1 << std::endl;
        } else{
            double error = analytic - res1;
            outfile << std::scientific << std::setprecision(this->precision);
            outfile << "k = " << i * this->frequency + 1 << " result = " << res1 << " ";
            outfile << "analytic: " << analytic << " ";
            outfile << "error: " << error << " ";
            outfile << "converge: " << converg << std::endl;
        }

        i += 1;
        res0 = res1;
    }

    outfile.close();
}

std::string WriteSeries ::setSeparator() {
    std::string filename = "result.txt"; //defaulted ".txt" type

    if (this -> seperator == ","){
        filename = "result.csv";
    } else if (this -> seperator == " "){
        filename = "result.txt";
    } else if (this -> seperator == "|"){
        filename = "result.psv";
    } else{
        filename = "result.txt";
    }
    
    return filename;
}