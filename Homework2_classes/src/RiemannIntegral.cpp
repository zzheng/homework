#include "RiemannIntegral.h"
#include <cmath>
#include <stdexcept>

RiemannIntegral::RiemannIntegral(double a, double b, std::string f) {
    this->a = a;
    this->b = b;
    this->f = f;
}

double RiemannIntegral::func(double x)  {

    if (this->f == std::string("x3")){
        return x * x * x;
    } else if (this->f == std::string("cosx")){
        return cos(x);
    } else if (this->f == std::string("sinx")){
        return sin(x);
    } else{
        throw std::invalid_argument("Please choose a function among x^3, cosx and sinx");
    }
}

double RiemannIntegral::compute(unsigned int N)  {
    double dx = (this->b - this->a) / N;
    double integral = 0;

    for (int i = 1; i <= N; ++i) {
        double xi = dx * i + this->a;
        integral += dx * this->func(xi);
    }
    return integral;
}

double RiemannIntegral::getAnalyticPrediction() {

    if (this->f == std::string("x3")){
        double res = 1./4. * (pow((this->b),4) - pow((this->a),4));
        return res;

    } else if (this->f == std::string("cosx")){
        double res = sin(this->b) - sin(this->a);
        return res;

    } else if (this->f == std::string("sinx")){
        double res = -cos(this->b) + cos(this->a);
        return res;

    } else{
        throw std::invalid_argument("Please choose among f x3, cosx and sinx");
    }
}
