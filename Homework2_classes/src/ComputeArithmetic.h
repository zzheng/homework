/** --------------------------------------------------------------------------
This file contains the ComputeArithmetic class inheriting from the Series class
-------------------------------------------------------------------------- */

#ifndef COMPUTEARITHMETIC_H
#define COMPUTEARITHMETIC_H

#include "Series.h"

class ComputeArithmetic : public Series {

public:
    // methods
    double getAnalyticPrediction() override;
    double computeTerm(unsigned int k) override;
};

#endif
