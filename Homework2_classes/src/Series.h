/** --------------------------------------------------------------------------
This file contains the Series class' declaration
-------------------------------------------------------------------------- */

#ifndef SERIES_H
#define SERIES_H

class Series {

public:
    // methods
    virtual double compute (unsigned  int N); // factorized according to exercise 5
    virtual double getAnalyticPrediction();
    void addTerm() ;  // added to prevent re-computation
    virtual double computeTerm(unsigned int k) {return 0;};
    
public:
    // members
    unsigned int current_index; 
    double current_value;
};

#endif
