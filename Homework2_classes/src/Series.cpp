#include <cmath>
#include "Series.h"


double Series ::compute(unsigned int N){
    //check if current index exceeds N, if so we restart the calculation
    if (this->current_index <= N){
        N -= this->current_index;
    } 
    else{
        this->current_index = 0;
        this->current_value = 0;
    }
    for (int i = 0; i < N; ++i) {
        this->addTerm();
    }
    return this->current_value;
}

double Series ::getAnalyticPrediction() {
    return std::nan(" ");
}

void Series ::addTerm() {
    this->current_index += 1;
    this->current_value += this->computeTerm(this->current_index);
}