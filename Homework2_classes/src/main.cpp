/** -------------------------------------------------------------------------- 
HW2 - Classes in C++ - Oct 2021

Students:
Zheng Zheng    zheng.zheng@epfl.ch
Tian Yang      tian.yang@epfl.ch
-------------------------------------------------------------------------- */

#include <iostream>
#include "ComputeArithmetic.h"
#include "ComputePi.h"
#include "PrintSeries.h"
#include <memory>
#include <string>
#include "WriteSeries.h"
#include <fstream>
#include <stdexcept>
#include "RiemannIntegral.h"
#include <iomanip>


int main(int argc, char ** argv) {

    //Exercise 2-5
    if (argv[1] == std::string("Pi") || argv[1] == std::string("Arithmetic")) {

        // If not enough arguments, stop the program.
        if (argc < 6) {
            throw std::invalid_argument("The number of arguments should be at least 5");
        }

        // Concatenate argv[]
        std::stringstream sstr;
        for (int i = 1; i < argc; i++) {
            sstr << argv[i] << " ";
        }

        unsigned int fre, max, pre; //fre: frequency, max: maxiter, pre:precision
        std::string ser, ope, sep; //ser: series type, ope: operation(Print/Write), sep: separator
        sstr >> ser >> fre >> max >> pre >> ope >> sep;

        //Exercise 2
        std::unique_ptr<Series> met = nullptr;
        if (ser == std::string("Arithmetic")) {
            met = std::make_unique<ComputeArithmetic>();
        } 
        
        else if (ser == std::string("Pi")) {
            met = std::make_unique<ComputePi>();
        } 
        
        else {
            throw std::invalid_argument("No such Series method");
        }

        //Exercise 3
        if (ope == std::string("Print")) {
            PrintSeries ps(fre, max, *met);
            ps.setPrecision(pre);
            ps.dump();
        } 
        
        else if (ope == std::string("Write")) {
            WriteSeries ws(fre, max, *met, sep);
            ws.setPrecision(pre);
            ws.dump();
        }

        //Exercise 4
        std::ofstream outfile;
        outfile.open("exercise5.txt", std::ios::trunc);
        PrintSeries ps5(fre, max, *met);
        ps5.setPrecision(pre);
        outfile << ps5; // Write results to "exercise5.txt" with overloaded "<<"

        //Exercise 5
        //Class "Series","ComputePi" and "ComputeArithmetic" are factorized
        //Re-computation is prevented after modification
    }

    //Exercise 6
    else if (argv[1] == std::string("Riemann")) {

        // if not enough arguments, stop the program.
        if (argc < 7) {
            throw std::invalid_argument("The number of arguments should be at least 5");
        }

        // Concatenate the arguments in argv[]
        std::stringstream sstr;
        for (int i = 1; i < argc; i++) {
            sstr << argv[i] << " ";
        }

        double a, b;
        unsigned int N, pre; 
        std::string ser, ftype; //ftype: function type
        sstr >> ser >> a >> b >> N >> pre >> ftype;

        auto integ = std::make_unique<RiemannIntegral>(a, b, ftype);

        std::cout << std::scientific << std::setprecision(pre);
        std::cout <<"Numerical: " << integ->compute(N) << std::endl;
        std::cout <<"Analytic: " << integ->getAnalyticPrediction() << std::endl;
    }
}
