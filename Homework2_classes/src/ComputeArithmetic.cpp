#include "ComputeArithmetic.h"


double ComputeArithmetic::getAnalyticPrediction() {
    return 0.5 * this->current_index * (this->current_index + 1);
}

double ComputeArithmetic::computeTerm(unsigned int k) {
    return 1.0 * k;
}
