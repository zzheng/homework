#include "ComputePi.h"
#include <cmath>


double ComputePi::compute(unsigned int N) {
    Series::compute(N);
    return std::sqrt(6. * this->current_value);
}

double  ComputePi::getAnalyticPrediction() {
    return M_PI;
}

double ComputePi::computeTerm(unsigned int k) {
    return 1./(1. * k * k);
}