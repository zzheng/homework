/** --------------------------------------------------------------------------
This file contains  PrintSeries class inheriting from the DumperSeries class
-------------------------------------------------------------------------- */

#ifndef PRINTSERIES_H
#define PRINTSERIES_H

#include "DumperSeries.h"

class PrintSeries : public DumperSeries{

public:
    // methods
    PrintSeries(unsigned int f, unsigned int m, Series &s);
    void dump(std::ostream &os = std::cout) override;

public:
    // members
    unsigned int frequency;
    unsigned int maxiter;
};

#endif
