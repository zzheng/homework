#include "PrintSeries.h"
#include <iostream>
#include <iomanip>
#include <cmath>


PrintSeries :: PrintSeries(unsigned int f, unsigned int m, Series &s):DumperSeries(s) {
    this->frequency = f;
    this->maxiter = m;
}

void PrintSeries :: dump(std::ostream &os)  {

    double res1 = 0; //res1 stores current result
    double res0 = 0; //res0 stores last result
    double converg = 0; //converg: Convergence

    int i = 0;
    while ( i * this->frequency + 1 < this->maxiter){
        res1 = this->series.compute(i * this->frequency + 1);
        converg = res1 - res0;
        double analytic = this->series.getAnalyticPrediction(); //analytic: analytic solution

        // if no analytic solution
        if (std::isnan(analytic)){ 
            os << "k = " << i * this->frequency + 1 << " result = " << res1 << std::endl;
        } 
        else{
            double error = analytic - res1;
            os << std::scientific << std::setprecision(this->precision);
            os << "k = " << i * this->frequency + 1 << " result = " << res1 << " ";
            os << "analytic: " << analytic << " ";
            os << "error: " << error << " ";
            os << "converge: " << converg << std::endl;
        }
        
        i += 1;
        res0 = res1;
    }
}
