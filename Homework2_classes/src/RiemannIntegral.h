/** --------------------------------------------------------------------------
This file contains RiemannIntegral class inheriting from the Series class
-------------------------------------------------------------------------- */
#ifndef RIEMANNINTEGRAL_H
#define RIEMANNINTEGRAL_H

#include "Series.h"
#include <string>

class RiemannIntegral: public Series {

public:
    // methods
    RiemannIntegral(double a, double b, std::string f);
    double func (double x) ;
    double compute (unsigned int N) override;
    double getAnalyticPrediction () override;
    
public:
    // members
    double a;
    double b;
    std::string f;
};

#endif
