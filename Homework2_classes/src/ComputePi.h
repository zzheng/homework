/** --------------------------------------------------------------------------
This file contains the ComputePi class inheriting from the Series class
-------------------------------------------------------------------------- */

#ifndef COMPUTEPI_H
#define COMPUTEPI_H

#include "Series.h"

class ComputePi : public Series{

public:
    // methods
    double compute(unsigned int N) override;
    double getAnalyticPrediction() override;
    double computeTerm(unsigned int k) override;
};

#endif
