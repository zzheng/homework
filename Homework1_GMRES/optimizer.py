"""
HW1 - GMRES in Python - Oct 2021
Exercise 1 

Students:
Zheng Zheng    zheng.zheng@epfl.ch
Tian Yang      tian.yang@epfl.ch
"""



# imports
import numpy as np
import scipy.optimize
import scipy.sparse.linalg
import matplotlib.pyplot as plt
from matplotlib import cm
import argparse

# target function for minimizing
def S_x(x, A, b):   # x: variable; A&b: matrix

    return 0.5*np.dot(x.T, A)@x - x@b

# recording the value of x in each iteration
def callbackfun(x):

    global x_iter  # x_iter is an array to store the value of x in each iteration
    x_iter = np.append(x_iter, x.reshape(1, -1), axis=0) # add the new value of x into the bottom of x_iter

# BFGS method to minimize the target function
def mini_BFGS(func, A, b, x_guess):  # func: target function; A,b matrix; x_guess: the initial guess of x

    global x_iter # x_iter is an array to store the value of x in each iteration
    x_iter = x_guess.reshape(1, -1)
    # apply BFGS method 
    res = scipy.optimize.minimize(fun = func, x0 = x_guess, args = (A,b), callback = callbackfun,
                              method = 'BFGS', tol = 1e-5, options = {'maxiter':10})
                              
    print('BFGS: {}'.format(res.message))
    print('Total iteration: {}'.format(x_iter.shape[0] - 1))
    print('x = {}'.format(x_iter[-1,:])) #print the message and the final value of x
    
    return x_iter

# LGMRES method
def solve_LGMRES(A, b, x_guess): 

    global x_iter 
    x_iter = x_guess.reshape(1, -1)
    # solve the function with LGMRES
    res, info = scipy.sparse.linalg.lgmres(A = A, b = b, x0 = x_guess, tol = 1e-5, atol = 1e-5, maxiter = 500, callback = callbackfun)
    
    if info == 0:
        print("LGMRES: Succeeded")
    elif info > 0:
        print("LGMRES: Convergence to tolerance not achievd")
    else:
        print("LGMRES: Illegal input or breakdown")
    print('Total iteration: {}'.format(x_iter.shape[0] - 1))
    print('x = {}'.format(x_iter[-1,:])) 
    
    return x_iter

# plot the figures of target function and iteration path
def plot3D(A, b, x, name):

    if b.shape[0] != 2: #only for 3D figures
        print('The dimension of system is not 3D')
        return
    else:
        fig = plt.figure()
        ax1 = fig.add_subplot(projection = '3d')
        
        nf = 200 #the number of sampling points in one direction
        X = np.linspace(-3.1, 3.1, nf)
        Y = np.linspace(-1.8, 4.4, nf)
        X,Y = np.meshgrid(X, Y) #generate the mesh
        Z = np.zeros((nf, nf)) #calculate z[i,j] = S(X[i,j],Y[i,j])
        for i in range(0, nf):
            for j in range(0, nf):
                Z[i,j] = S_x(np.array([X[i,j],Y[i,j]]),A,b)
                
        # plot the surface of S(x)
        ax1.plot_surface(X, Y, Z, rstride=10, cstride=10,
                               linewidth = 0, alpha = 0.5, cmap = cm.coolwarm)
        # plot the contour of S(x) in 3D
        ax1.contour3D(X, Y, Z,stride = 1, colors = 'b')
        # plot the iteration path
        z1 = np.zeros(x.shape[0])
        for i in range(0, x.shape[0]):
            z1[i] = S_x(x[i,:], A, b)
        ax1.plot(x[:,0],x[:,1], z1, marker='.',
                 c = 'r', ls = '--', label = 'Iteration Path')
                 
        # plot setting
        ax1.legend(loc = "upper left")
        ax1.view_init(elev = 65, azim = 60)
        ax1.set_xlim(-3, 3)
        ax1.set_ylim(-1.7, 4.3)
        ax1.set_zlim(0, 70)
        ax1.set_xlabel('x1')
        ax1.set_ylabel('x2')
        ax1.set_zlabel('S(x)')
        ax1.set_title(name)
        ax1.set_zticks(np.arange(0, 70, 20))
        plt.show()

if __name__ == '__main__' : 

    # argparse setting
    parser = argparse.ArgumentParser()
    
    #<required>input the value of A through command-line
    parser.add_argument("-A", type=str, required = True,
                    help="<Required> the matrix A, Correct input format: '1, 2; 3 4'")
    #<required>input the value of b through command-line
    parser.add_argument("-b", type=float, required = True, nargs="+",
                    help="<Required> the vector b, Input format: 1 2")
    parser.add_argument("-p", "--plot", action="store_true",  #plot the figure or not
                    help="Plot the result or not (only for 3D systems)")
    parser.add_argument("--solver", default='LGMRES',const='LGMRES', nargs = '?', #choose the solver, default = BFGS
                    choices=['BFGS', 'LGMRES'], help = 'Default = LGMRES, Choose the solver you want')
    args = parser.parse_args()
    
    # Get the value of A, b from command-line
    A = np.array(np.matrix(args.A))
    b = np.array(args.b)
    x0 = np.zeros(b.shape[0]) #initial guess of x
    
    # according to the chosen solver, implement the right function
    if args.solver == 'BFGS':
        x = mini_BFGS(S_x, A, b, x0)
    elif args.solver == 'LGMRES':
        x = solve_LGMRES(A, b, x0)
    if args.plot: #plot the figure or not
        plot3D(A, b, x, args.solver)
        
        
        
