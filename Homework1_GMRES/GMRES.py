"""
HW1 - GMRES in Python - Oct 2021
Exercise 2 
GMRES iteratively approaches the solution from the initial guess of the linear system Ax = b

Students:
Zheng Zheng    zheng.zheng@epfl.ch
Tian Yang      tian.yang@epfl.ch
"""

# imports
import numpy as np
import argparse
import optimizer # import the optimizer.py program we coded in exercise 1

# =============================================================================
#   GMRES method to solve x
# =============================================================================

def GMRES(A, b, x_guess_ini, max_iter, tol) : # A,b matrix; max_iter: max iteration number; tol: tolerance of error

    r = b - np.einsum('ij,j->i', A, x_guess_ini) 
    x = np.array([x_guess_ini]).reshape(1,-1) # x is an array to store the value of x in each iteration
    H = np.zeros((2,1)) # H is the Hessenberg matrix initialization
    Q = (r/np.linalg.norm(r)).reshape(-1,1)  # Q is the matrix formed by orthonormal vector q1, q2,...qk
    
    for k in range(max_iter):
        Q, H = Arnold_Iteration(A, Q, H, k) # Arnold iteration
        beta = np.zeros(H.shape[0]) # define the size of beta*e1
        beta[0] = np.linalg.norm(r) # define the value of beta*e1
        yn = np.linalg.lstsq(H,beta,rcond=None)[0] # compute the value of min(yn) = ||Hn*yn - beta*e1|| by Least square method
        xn = np.einsum('ij,j->i', Q[:,:-1], yn) + x_guess_ini # compute the value of xn in kth iteration with yn, Q
        x = np.append(x, xn.reshape(1,-1), axis=0) # add xn to the bottom of array x
        rn = np.einsum('ij,j->i', A, xn) - b # compute the current residual error
        
        if np.linalg.norm(rn)/np.linalg.norm(xn) <= tol: # if rn is less than the tol we set
            print("Converged successfully")
            print("Total iteration: {}" .format(k+1))
            print('x = {}'.format(x[-1,:]))  # print message and the final value of x
            return x
        elif k == max_iter - 1: # if the tolerance is not achieved within max_iter
            print("Not converged for the defined max_iter")

# =============================================================================
#   Arnoldi iteration, Gram-Schmidt method
# =============================================================================

def Arnold_Iteration (A, Q, H, k):

    q = np.einsum('ij,j->i',A,Q[:,k]) # q stands for the (k+1)th orthnormal vector for t Krylov subspace
    
    if k!= 0: # modify the size of H for kth iteration
        H = np.append(H, np.zeros((H.shape[0], 1)), axis = 1)
        H = np.append(H, np.zeros((1, H.shape[1])), axis = 0)
        
    for i in range(k + 1): # calculate H and q
        H[i,k] = np.einsum('i,i', q, Q[:,i])
        q = q - H[i,k]*Q[:,i]
    H[k + 1, k] = np.linalg.norm(q)
    q = q/H[k + 1, k]
    Q = np.append(Q, q.reshape(-1,1), axis = 1) # add q as the rightest column of Q
    return Q, H

if __name__ == '__main__' : 

    # argparse setting
    parser = argparse.ArgumentParser()
    #<required>input the value of A through command line
    parser.add_argument("-A", type=str, required = True,
                    help="<Required> the matrix A, Input format: '1, 2; 3 4'")
    #<required>input the value of b through command line
    parser.add_argument("-b", type=float, required = True, nargs="+",
                    help="<Required> the vector b, Input format: 1 2")
    parser.add_argument("-p", "--plot", action="store_true",
                    help="Plot the result or not (only for 3D systems)")
    parser.add_argument("--solver", default='GMRES',const='GMRES', nargs = '?',
                    choices=['GMRES', 'BFGS', 'LGMRES'], help = 'Default = GMRES, Choose the solver you want')
    args = parser.parse_args()
    
    # Get the value of A, b from command line
    A = np.array(np.matrix(args.A))
    b = np.array(args.b)
    x_guess_ini = np.zeros(b.shape[0]) #initial guess of x
    
    # according to the chosen solver, implement the right function
    if args.solver == 'GMRES':
        x = GMRES(A, b, x_guess_ini, 5, 1e-5)
    elif args.solver == 'BFGS': # implement functions from optimizer.py
        S_x = optimizer.S_x(x_guess_ini, A, b)
        x = optimizer.mini_BFGS(S_x, A, b, x_guess_ini)
    elif args.solver == 'LGMRES': 
        x = optimizer.solve_LGMRES(A, b, x_guess_ini)
    if args.plot:
        optimizer.plot3D(A, b, x, args.solver)



















