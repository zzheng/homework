-------------------------------------------------------------------
# Project Homework 1, Oct 2021
Authors:
Zheng Zheng    zheng.zheng@epfl.ch
Tian Yang      tian.yang@epfl.ch

-------------------------------------------------------------------
# Codes:
optimizer.py -> exercise 1

GMRES.py -> exercise 2

-------------------------------------------------------------------
# Dependencies: 
numpy, scipy, matplotlib, argparse

-------------------------------------------------------------------
# Execution:
To execute the program, type in the terminal:
$ python3 name.py -A '8 1; 1 3' -b 2 4 

To plot the figure, add -p 

To choose BFGS/LGMRES/GMRES solver, add --solver BFGS/LGMRES/GMRES

-------------------------------------------------------------------




